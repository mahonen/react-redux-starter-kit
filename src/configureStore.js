import { applyMiddleware, createStore, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import initialState from './initialState'

const createRootReducer = history =>
	combineReducers({
		router: connectRouter(history),
	})

const configureStore = history => {
	const store = createStore(
		createRootReducer(history),
		composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk)),
	)

	if (module.hot) {
		module.hot.accept(createRootReducer, () => {
			store.replaceReducer(createRootReducer(history))
		})
	}
	return store
}

export default configureStore
