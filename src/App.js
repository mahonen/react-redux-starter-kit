import React from 'react'
import { Route, Switch } from 'react-router'
import Root from './Root'

const App = () => (
	<Root>
		<Switch>
			<Route exact path="/" render={() => 'Hello world!'} />
		</Switch>
	</Root>
)

export default App
