import React from 'react'
import { Provider } from 'react-redux'
import { createBrowserHistory } from 'history'
import { ConnectedRouter } from 'connected-react-router'
import configureStore from './configureStore'

const history = createBrowserHistory()
const store = configureStore(history)

export default ({ children }) => (
	<Provider store={store}>
		<ConnectedRouter history={history}>{children}</ConnectedRouter>
	</Provider>
)
