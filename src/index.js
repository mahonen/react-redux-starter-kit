import { render } from 'react-dom'
import React from 'react'
import App from './App'

const renderApp = component => {
	render(component, document.getElementById('app-root'))
}

renderApp(<App />)

/* eslint global-require: "off" */
module.hot.accept('./App', () => {
	const NextApp = require('./App').default
	renderApp(<NextApp />)
})
