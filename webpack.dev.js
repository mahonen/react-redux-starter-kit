const merge = require('webpack-merge')
const webpack = require('webpack')
const common = require('./webpack.common.js')

module.exports = merge(common, {
	mode: 'development',
	devtool: 'cheap-module-source-map',
	devServer: {
		contentBase: './dist',
		compress: true,
		stats: 'errors-only',
		host: '0.0.0.0',
		historyApiFallback: true,
	},
	plugins: [new webpack.NamedModulesPlugin()],
})
