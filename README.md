### Scaffolding for React projects
 - React
 - Redux (thunk for async, DevTools extension for debugging)
 - Axios for requests
 - React Router with Redux bindings
 - Webpack, configured with error layout, hot reload, tree shaking, code optimizations
 - Linting by ESLint (modified AirBnb style), formatting by Prettier
 - Jest and Enzyme
 - Styled-components for styling
 - Babel configured for React and Generators, Async/Await, Class properties

#### Scripts:
Install:
```
yarn
```
Start developing!
```
yarn dev
```

Run tests
```
yarn test:watch
```

Bundle for production:
```
yarn build
```
